using System.IO;
using Unity.Build.Common;
using Unity.Build.Classic.Private;

namespace Unity.Build.Linux.Classic
{
    sealed class LinuxClassicNonIncrementalPipeline : ClassicNonIncrementalPipelineBase
    {
        public override Platform Platform { get; } = Platform.Linux;

        public override BuildStepCollection BuildSteps { get; } = new[]
        {
            typeof(SaveScenesAndAssetsStep),
            typeof(ApplyUnitySettingsStep),
            typeof(BuildPlayerStep),
            typeof(CopyAdditionallyProvidedFilesStep),
            typeof(LinuxProduceArtifactStep)
        };

        protected override ResultBase OnCanRun(RunContext context)
        {
            var artifact = context.GetBuildArtifact<LinuxArtifact>();
            if (artifact == null)
            {
                return Result.Failure($"Could not retrieve build artifact '{nameof(LinuxArtifact)}'.");
            }

            if (artifact.OutputTargetFile == null)
            {
                return Result.Failure($"{nameof(LinuxArtifact.OutputTargetFile)} is null.");
            }

            if (!File.Exists(artifact.OutputTargetFile.FullName))
            {
                return Result.Failure($"Output target file '{artifact.OutputTargetFile.FullName}' not found.");
            }

            return Result.Success();
        }

        protected override RunResult OnRun(RunContext context)
        {
            return LinuxRunInstance.Create(context);
        }

        protected override void PrepareContext(BuildContext context)
        {
            base.PrepareContext(context);
            var classicData = context.GetValue<ClassicSharedData>();
            classicData.StreamingAssetsDirectory = $"{context.GetOutputBuildDirectory()}/{context.GetComponentOrDefault<GeneralSettings>().ProductName}_Data/StreamingAssets";
        }
    }
}
