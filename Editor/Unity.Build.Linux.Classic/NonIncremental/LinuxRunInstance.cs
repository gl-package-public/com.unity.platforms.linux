using System.Diagnostics;
using Unity.Build.Desktop.Classic;

namespace Unity.Build.Linux.Classic
{
    sealed class LinuxRunInstance : ClassicDesktopRunInstance
    {
        protected override ProcessStartInfo GetStartInfo(RunContext context)
        {
            var startInfo = base.GetStartInfo(context);
            var artifact = context.GetBuildArtifact<LinuxArtifact>();

            startInfo.FileName = artifact.OutputTargetFile.FullName;
            startInfo.WorkingDirectory = artifact.OutputTargetFile.Directory?.FullName ?? string.Empty;
            return startInfo;
        }

        public LinuxRunInstance(RunContext context) : base(context)
        {
        }

        public static RunResult Create(RunContext context)
        {
            return new LinuxRunInstance(context).Start(context);
        }
    }
}
