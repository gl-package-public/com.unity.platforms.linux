﻿using Unity.Build.Classic;
using Unity.Build.Editor;
using UnityEditor;

namespace Unity.Build.Linux.Classic
{
    static class LinuxMenuItem
    {
        [MenuItem(BuildConfigurationMenuItem.k_MenuPathName + KnownPlatforms.Linux.DisplayName + ClassicBuildConfigurationMenuItem.k_ItemNameSuffix)]
        static void CreateClassicBuildConfigurationAsset()
        {
            ClassicBuildConfigurationMenuItem.CreateAssetInActiveDirectory(Platform.Linux);
        }
    }
}
