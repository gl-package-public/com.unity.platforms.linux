using NUnit.Framework;
using Unity.Build.Editor;
using Unity.Serialization.Json;

namespace Unity.Build.Linux.Tests
{
    class LinuxPlatformTests
    {
        [Test]
        public void LinuxPlatform_Equals()
        {
            var platform = new LinuxPlatform();
            Assert.That(platform, Is.EqualTo(Platform.Linux));
        }

        [Test]
        public void LinuxPlatform_GetIcon()
        {
            Assert.That(Platform.Linux.GetIcon(), Is.Not.Null);
        }

        [Test]
        public void LinuxPlatform_Serialization()
        {
            var serialized = JsonSerialization.ToJson(Platform.Linux);
            var deserialized = JsonSerialization.FromJson<Platform>(serialized);
            Assert.That(deserialized, Is.EqualTo(Platform.Linux));
        }
    }
}
