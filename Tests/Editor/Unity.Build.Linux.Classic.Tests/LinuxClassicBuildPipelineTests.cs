using NUnit.Framework;
using Unity.Build.Classic.Private;

namespace Unity.Build.Linux.Classic.Tests
{
    [TestFixture]
    class LinuxClassicBuildPipelineTests
    {
        [Test]
        public void BuildPipelineSelectorTests()
        {
            var selector = new BuildPipelineSelector();
            Assert.That(selector.SelectFor(Platform.Linux).GetType(), Is.EqualTo(typeof(LinuxClassicNonIncrementalPipeline)));
        }
    }
}
